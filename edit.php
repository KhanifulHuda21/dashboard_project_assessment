<?php 

include 'connection.php';

$nama=$db->query("select * from buku where id=".$_GET["id"]);

$data_library=$nama->fetchAll();

?>


<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
<title>Edit Data</title>
<style>
  body{
     background: url(images/logo.png) no-repeat fixed;
     background-color: whitesmoke;
     background-size: 500px;
     background-position-x:425px;
     background-position-y: 60px;
     }
  #col{
    background-color:grey;
    opacity:0.9;
    filter:alpha(opacity=60); 
    color: white;
    font-size:20px;
    
    }
</style>
</head>
<body>
<!-- -------------------------Form Edit Data------------------------- -->
<div class="container" id="inputmodal">
    <div class="row vh-100 justify-content-center center-fixed">
        <div class="col-10 border rounded p-3 align-self-center" id="col" >
            <form action="update.php" method="POST">
                <h2>Edit Data</h2>
            <input type="hidden" name="id" value="<?php echo $data_library[0]["id"]; ?>">
            <div class="form-group">
                <label for="exampleInputEmail1">Judul</label>
                <input type="text" name="judul" value="<?php echo $data_library[0]["judul"]; ?>" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Pengarang</label>
                <input type="text" name="pengarang" value="<?php echo $data_library[0]["pengarang"]; ?>" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Penerbit</label>
                <input type="text" name="penerbit" value="<?php echo $data_library[0]["penerbit"]; ?>" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">ISBN</label>
                <input type="text" name="isbn" value="<?php echo $data_library[0]["isbn"]; ?>" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Jumlah</label>
                <input type="number" name="jumlah" value="<?php echo $data_library[0]["jumlah"]; ?>" class="form-control" id="exampleInputPassword1">
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
            <a href="data.php" type="button" class="btn btn-dark">Batal</a>
            </form>
        </div>
    </div>
</div>

<!-- Option 2: jQuery, Popper.js, and Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

</body>
</html>