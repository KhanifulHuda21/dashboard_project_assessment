<?php 

include "connection.php";
include "search.php";

$nama=$db->query("select * from buku");
$data_library=$nama->fetchAll();

foreach ($data_library as $key) {

}

// search pencarian
$penampung=[];
if(isset($_POST['cari']))
{
  $penampung=searching($_POST['search']);
}
if (!empty($penampung))
{
  $data_library=$penampung;
}


// Search Action
if(isset($_POST['search']))
{


  $filter=$db->quote($_POST['search']);  

  $name=$_POST['search'];

  $search=$db->prepare("select * from buku where judul=? or pengarang=? or penerbit=? or jumlah=?");

  $search->bindValue(1,$name,PDO::PARAM_STR);
  $search->bindValue(2,$name,pdo::PARAM_STR);
  $search->bindValue(3,$name,pdo::PARAM_STR);
  $search->bindValue(4,$name,pdo::PARAM_INT);

  $search->execute();

  $tampil_data=$search->fetchAll(); 

  $row = $search->rowCount();
}else{
  $data = $db->query("select * from buku");

  $tampil_data = $data->fetchAll();
}

?>

<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/solid.css" integrity="sha384-yo370P8tRI3EbMVcDU+ziwsS/s62yNv3tgdMqDSsRSILohhnOrDNl142Df8wuHA+" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/brands.css" integrity="sha384-/feuykTegPRR7MxelAQ+2VUMibQwKyO6okSsWiblZAJhUSTF9QAVR0QLk6YwNURa" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/fontawesome.css" integrity="sha384-ijEtygNrZDKunAWYDdV3wAZWvTHSrGhdUfImfngIba35nhQ03lSNgfTJAKaGFjk2" crossorigin="anonymous">
<link rel="stylesheet" href="css/styles.css">
<link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
<title>dashboard</title>
<style>
  body{
    background-image: url(images/book.jpg);
    background-size: cover;
    background-position: center;
    
  
  }
</style>
</head>
<body>

<!-- -------------------------Navbar------------------------- -->
<nav class="navbar navbar-light bg-dark col-12 col-sm-12 col-md-12 col-lg-12" id="nav" style="z-index: 1;" >
  <h3 class="text-light" style="font-size:25px; font-family:calibry">Pustaka.com</h3>
  <div class="form-inline">
  <div style="font-size:15px;"><i class="fas fa-clock text-light"></i> <span class="jam text-light" ></span></div>
</div>
</nav>

<!-- -------------------------Sidebar------------------------- -->
<div class="sidebar border rounded col-5 col-sm-5 col-md-12 ">
  <div class=" text-center">
    <img src="images/logo.png" alt="" class="mt-3" width="100px" height="100px">
    <h4 class="text-light">Pustaka.com</h4><hr class="bg-secondary">
  </div>
  <p class="text-white ml-3 mt-3"class="text-light">~My App~</p>
  <a class="text-white" href="index.php" class="text-light"><i class="fas fa-home mr-2"></i> Home</a>
  <a class="text-white" href="data.php" class="text-light"><i class="fas fa-table"></i> Data</a>
  <a class="text-white" href="dashboard.php" class="text-light"><i class="fas fa-box"></i> Dashboard</a>
  <p id="copyRight">&copy;KhanifulHuda | 2020</p>
</div>

<div class="container" id="dasHeader">
    <div class="row">
        <div class="col">
        <div class="card  text-white"  style=" background-color:#0e0d0d; opacity:0.6; filter:alpha(opacity=60); ">
  <img src="images/background.png" height="200px" class="card-img" alt="...">
  <div class="card-img-overlay">
    <h1 class="card-title"> Pustaka.com Dashboard</h1>
    <p class="card-text">Ini adalah dashboard untuk mengetahui atau mencari sebuah data  yang valid</p>
    <form class="form-inline" action="dashboard.php" method="POST">
    <input class="form-control mr-sm-2 "  type="search" name="search" placeholder="Cari Data" autocomplete="off" aria-label="Search" size="60px">
    <button class="btn btn-outline-light my-2 my-sm-0" name="cari" type="submit"><i class="fas fa-search"></i></button>
  </form>
    <!-- Allert Massage -->
    <?php if(isset($row)):?>
      <div class="alert alert-primary alert-dismissible fade show" role="alert">
        <p class="lead "><?php echo $row;?> Data Ditemukan !</p>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
      <?php endif; ?>
  </div>
</div>
        </div>
    </div>
</div>

<div class="container" id="dasContent">
  <div class="row ">
    <div class="col-7 border rounded p-2 mx-3" id="xxx" style=" background-color:#0e0d0d; opacity:0.6; filter:alpha(opacity=60); ">
      <!-- table content -->
      <table class="table">
        <thead>
        <tr>
          <th scope="col"class="text-light">No</th>
          <th scope="col" class="text-light">Judul</th>
          <th scope="col" class="text-light">Pengarang</th>
          <th scope="col" class="text-light">Penerbit</th>
          <th scope="col" class="text-light">ISBN</th>
          <th scope="col" class="text-light">Jumlah</th>
        </tr>
        </thead>
        <tbody>
        <?php $i=1 ;?>
        <?php foreach ($data_library as $key) : ?>
          <tr>
            <td class="text-light"><?php echo $i; ?></td>
            <td  class="text-light"><?php echo $key["judul"]; ?></td>
            <td  class="text-light"><?php echo $key["pengarang"]; ?></td>
            <td  class="text-light"><?php echo $key["penerbit"]; ?></td>
            <td  class="text-light"><?php echo $key["isbn"]; ?></td>
            <td  class="text-light"><?php echo $key["jumlah"]; ?></td>
          </tr>
          <?php $i++ ;?>
            <?php endforeach; ?>
          </tbody>
    </table>
    </div>
    <div class="col-4 border rounded"  style=" background-color:#0e0d0d; opacity:0.6; filter:alpha(opacity=60); ">  
      <small class = "text-light " style = "font-size: 13px;">
        <h6 class = "my-2"> Tentang data ini </h6>
        Data ini merupakan pendataan sebuah buku yang dilakukan secara digital.
        Pengguna dapat mendapatkan data yang valid dari sini.
        Dan mungkin akan berubah setiap saat.
      </small>
      <img class="mt-3" src="images/footer.png" height="170px" width="170px" alt="gambar gagal di proses" style="margin-left:90px;">
    </div>
  </div>
</div>


 <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</body>
</html>

<script type="text/javascript">
    function jam() {
    var time = new Date(),
        hours = time.getHours(),
        minutes = time.getMinutes(),
        seconds = time.getSeconds();
    document.querySelectorAll('.jam')[0].innerHTML = harold(hours) + ":" + harold(minutes) + ":" + harold(seconds);
      
    function harold(standIn) {
        if (standIn < 10) {
          standIn = '0' + standIn
        }
        return standIn;
        }
    }
    setInterval(jam, 1000);
</script>