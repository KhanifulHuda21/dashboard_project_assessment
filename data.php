<?php 

include "connection.php";
include "search.php";

$nama=$db->query("select * from buku");
$data_library=$nama->fetchAll();

foreach ($data_library as $key) {

}

// search pencarian
$penampung=[];
if(isset($_POST['cari']))
{
  $penampung=searching($_POST['search']);
}
if (!empty($penampung))
{
  $data_library=$penampung;
}


// Search Action
if(isset($_POST['search']))
{


  $filter=$db->quote($_POST['search']);  

  $name=$_POST['search'];

  $search=$db->prepare("select * from buku where judul=? or pengarang=? or penerbit=? or jumlah=?");

  $search->bindValue(1,$name,PDO::PARAM_STR);
  $search->bindValue(2,$name,pdo::PARAM_STR);
  $search->bindValue(3,$name,pdo::PARAM_STR);
  $search->bindValue(4,$name,pdo::PARAM_INT);

  $search->execute();

  $tampil_data=$search->fetchAll(); 

  $row = $search->rowCount();
}else{
  $data = $db->query("select * from buku");

  $tampil_data = $data->fetchAll();
}
?>

<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/solid.css" integrity="sha384-yo370P8tRI3EbMVcDU+ziwsS/s62yNv3tgdMqDSsRSILohhnOrDNl142Df8wuHA+" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/brands.css" integrity="sha384-/feuykTegPRR7MxelAQ+2VUMibQwKyO6okSsWiblZAJhUSTF9QAVR0QLk6YwNURa" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/fontawesome.css" integrity="sha384-ijEtygNrZDKunAWYDdV3wAZWvTHSrGhdUfImfngIba35nhQ03lSNgfTJAKaGFjk2" crossorigin="anonymous">
<link rel="stylesheet" href="css/styles.css">
<link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
<title>Data</title>
</head>
<body>

<!-- -------------------------Navbar------------------------- -->
<nav class="navbar navbar-light bg-dark col-12 col-sm-12 col-md-12 col-lg-12" id="nav" style="z-index: 1;" >
  <h3 class="text-light" style="font-size:25px; font-family:calibry">Pustaka.com</h3>
  <form class="form-inline" action="data.php" method="POST">
    <input class="form-control mr-sm-2 " type="search" name="search" placeholder="Cari Data" autocomplete="off" aria-label="Search">
    <button class="btn btn-outline-light my-2 my-sm-0" name="cari" type="submit"><i class="fas fa-search"></i></button>
  </form>
</nav>

<!-- -------------------------Sidebar------------------------- -->
<div class="sidebar col-5 col-sm-5 col-md-12 ">
  <div class=" text-center">
    <img src="images/logo.png" alt="" class="mt-3" width="100px" height="100px">
    <h4 class="text-light">Pustaka.com</h4><hr class="bg-secondary">
  </div>
  <p class="text-white ml-3 mt-3"class="text-light">~My App~</p>
  <a class="text-white" href="index.php" class="text-light"><i class="fas fa-home mr-2"></i>home</a>
  <a class="text-white" href="data.php" class="text-light"><i class="fas fa-table"></i> Data</a>
  <a class="text-white" href="dashboard.php" class="text-light"><i class="fas fa-box"></i>  Dashboard</a>
  <a class=" text-white" href="#" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-user-plus mr-2"></i>tambah data</a>    
  <p id="copyRight">&copy;KhanifulHuda | 2020</p>
</div>

<!-- -------------------------Page Contet------------------------- -->
<div class="content">
  <div class="row" style="z-index: -1;">
    <div class="col col-5 col-sm-5 col-lg-12">
      <!-- Allert Massage -->
  <?php if(isset($row)):?>
      <div class="alert alert-primary alert-dismissible fade show" role="alert">
        <p class="lead "><?php echo $row;?> Data Ditemukan !</p>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
      <?php endif; ?>
      <!-- table content -->
      <table class="table table-striped">
      <thead class="bg-secondary">
      <tr>
        <th scope="col"class="text-light">No</th>
        <th scope="col" class="text-light">Judul</th>
        <th scope="col" class="text-light">Pengarang</th>
        <th scope="col" class="text-light">Penerbit</th>
        <th scope="col" class="text-light">ISBN</th>
        <th scope="col" class="text-light">Jumlah</th>
        <th scope="col" class="text-light">Aksi</th>
      </tr>
      </thead>
      <tbody>
      <?php $i=1 ;?>
      <?php foreach ($data_library as $key) : ?>
        <tr>
          <td><?php echo $i; ?></td>
          <td><?php echo $key["judul"]; ?></td>
          <td><?php echo $key["pengarang"]; ?></td>
          <td><?php echo $key["penerbit"]; ?></td>
          <td><?php echo $key["isbn"]; ?></td>
          <td><?php echo $key["jumlah"]; ?></td>
          <td><a class="btn btn-danger"  href="delete.php?id=<?php echo $key["id"]; ?>"><i class="fas fa-user-times"></i></a> |
          <a class="btn btn-warning" href="edit.php?id=<?php echo $key["id"]; ?>"><i class="fas fa-user-edit"></i></a></td>
        </tr>
        <?php $i++ ;?>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- -------------------------Modal Form input Data------------------------- -->
<div class="modal fade col-10 col-sm-10 col-md-12 " id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Masukan Data</h5>
      </div>
      <div class="modal-body">
        <form action="input.php" method="POST">
          <div class="form-group">
            <label for="exampleInputEmail1">Judul</label>
            <input type="text" name="judul" autocomplete="off" required class="form-control">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Pengarang</label>
            <input type="text" name="pengarang" autocomplete="off" required class="form-control">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Penerbit</label>
            <input type="text" name="penerbit" autocomplete="off" required class="form-control">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">ISBN</label>
            <input type="text" name="isbn" autocomplete="off" required class="form-control">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Jumlah</label>
            <input type="number" name="jumlah" autocomplete="off" required class="form-control">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button> 
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

 <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</body>
</html>